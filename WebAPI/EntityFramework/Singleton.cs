﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.EntityFramework
{
    public sealed class Singleton
    {
        private Singleton()
        { }

        private static Singleton instance = null;
        private static readonly object obj = new object();
        private static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (obj)
                    {
                        if (instance == null)
                        {
                            instance = new Singleton();
                        }
                    }
                }

                return instance;
            }
        }       
    }
}
