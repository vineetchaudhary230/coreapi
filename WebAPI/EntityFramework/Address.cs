﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebAPI.EntityFramework
{
    public partial class Address
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
