﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebAPI.EntityFramework
{
    public partial class Employee
    {
        public Employee()
        {
            Addresses = new HashSet<Address>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
