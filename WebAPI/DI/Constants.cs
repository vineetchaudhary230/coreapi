﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DI
{
    public class Constants
    {
    }

    public enum CartSource
    {
        Cache = 1,
        DB = 2,
        API = 3
    }
}
