﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.EntityFramework;
using WebAPI.Models;


namespace WebAPI.DI
{
    public class UserRepository : IUserRepository
    {
        private TestContext ef;
        public UserRepository(TestContext _ef)
        {
            ef = _ef;
        }

        public bool ValidateUser(string username)
        {
            var data = ef.Employees.Where(x => x.UserName == username).FirstOrDefault();

            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<EmpAddressModel> GetAddress(string username)
        {
            var data = (from emp in ef.Employees.Where(x => x.UserName == username)
                        join add in ef.Addresses on emp.Id equals add.EmployeeId
                        select new EmpAddressModel()
                        {
                            Name = emp.FirstName + " " + emp.LastName,
                            City = add.City,
                            Id = add.Id,
                            State = add.State,
                            Pincode = add.Pincode
                        })
            .ToList();
            return data;
        }
        public bool AddUser(Employee emp)
        {
            Employee emps = new Employee();

            emps.FirstName = emp.FirstName;
            emps.LastName = emp.LastName;
            emps.Email = emp.Email;
            emps.UserName = emp.UserName;


            ef.Employees.Add(emps);
            ef.SaveChanges();
            return true;
        }
    }
}
