﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DI
{
    public interface IShoppingCart
    {
        object GetCart();
    }

}
