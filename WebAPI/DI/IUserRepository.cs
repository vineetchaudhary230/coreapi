﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.EntityFramework;
using WebAPI.Models;

namespace WebAPI.DI
{
    public interface IUserRepository
    {
        bool ValidateUser(string username);
        List<EmpAddressModel> GetAddress(string username);
        bool AddUser(Employee emp);
    }
}
