﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPI.DI;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        private readonly Func<string, IShoppingCart> _shoppingCart;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, Func<string, IShoppingCart> shoppingCart)
        {
            _logger = logger;
            this._shoppingCart = shoppingCart;
            Console.WriteLine();
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogDebug("debugging here");
            //throw new ArgumentException("error due to argument validation");


            _shoppingCart("DB").GetCart();



            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = 55,
                Summary = Summaries[1]
            })
            .ToArray();

        }
    }
}
