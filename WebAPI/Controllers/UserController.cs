﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPI.DI;
using WebAPI.EntityFramework;
using WebAPI.Models;
using Microsoft.ApplicationInsights;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //private EFContext db;
        private IUserRepository userRepo;
        public UserController(IUserRepository _userRepo)
        {
            userRepo = _userRepo;
        }

        // GET api/<UserController>/5
        [HttpGet]
        [Route("GetValidateUser/{username}")]
        public bool Get(string username)
        {
            TelemetryClient tt = new TelemetryClient();
            IDictionary<string, string> dct = new Dictionary<string, string>();
            dct.Add("user", username);
            tt.TrackEvent("testEvent", dct);
            return userRepo.ValidateUser(username);
        }

        [HttpGet]
        [Route("GetEmpAddress/{username}")]
        public List<EmpAddressModel> GetEmpAddress(string username)
        {
            return userRepo.GetAddress(username);
        }

        // POST api/<UserController>
        [Route("adduser")]
        [HttpPost]
        //[EnableCors("AnotherPolicy")]
        public bool AddUser(Employee user)
        {
            return userRepo.AddUser(user);
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
